/* eslint-disable @typescript-eslint/parameter-properties */
// eslint-disable-next-line import/no-unresolved
import { TreeItem } from "vscode";
import type { Command, TreeItemCollapsibleState } from "vscode";

class FindingTreeItem extends TreeItem {
  public contextValue = "finding";

  public constructor(
    public readonly label: string,
    public readonly collapsibleState: TreeItemCollapsibleState,
    public readonly command?: Command
  ) {
    super(label, collapsibleState);
  }
}

export { FindingTreeItem };
