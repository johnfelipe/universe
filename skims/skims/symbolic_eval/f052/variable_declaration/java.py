import ctx
from model.graph import (
    Graph,
    NId,
)
from pathlib import (
    Path,
)
import re
from symbolic_eval.common import (
    INSECURE_HASHES,
)
from symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
import utils.graph as g


def get_first_string(graph: Graph, n_id: NId) -> str | None:
    if first_str := g.match_ast_d(graph, n_id, "Literal", -1):
        return graph.nodes[first_str].get("value")
    return None


def infere_path(file_name: str) -> str | None:
    for config_path in ctx.SKIMS_CONFIG.sast.include:
        if config_path[0] == "/":
            file_path = config_path
        else:
            file_path = str(
                Path(ctx.SKIMS_CONFIG.working_dir, config_path).resolve()
            )

        if file_path.split("/")[-1] == file_name:
            return file_path
    return None


def get_file_names(args: SymbolicEvalArgs) -> set[str]:
    graph = args.graph
    var_name_matcher = re.compile(r"loader_name__.+")
    loader_vars_raw = filter(var_name_matcher.match, args.triggers)
    loader_vars = [*map(lambda x: x.split("__")[1], loader_vars_raw)]

    loader_nodes: set[NId] = set()

    for n_id in g.matching_nodes(
        graph, label_type="MethodInvocation", expression="load"
    ):
        if (obj_id := graph.nodes[n_id].get("object_id")) and (
            graph.nodes[obj_id].get("symbol") in loader_vars
        ):
            loader_nodes.add(n_id)

    loader_file_names: set[str] = set()
    for loader_node in loader_nodes:
        if (file_name := get_first_string(graph, loader_node)) and (
            ".properties" in file_name
        ):
            loader_file_names.add(file_name[1:-1])
    return loader_file_names


def get_file_paths(args: SymbolicEvalArgs) -> set[str]:
    file_paths: set[str] = set()
    for file_name in get_file_names(args):
        if file_path := infere_path(file_name):
            file_paths.add(file_path)
    return file_paths


def evaluate_properties(args: SymbolicEvalArgs, load_key: str) -> str | None:
    for path in get_file_paths(args):
        with open(path, "r", encoding="utf-8") as prop_file:
            for line in prop_file:
                line_split = line.split("=")
                if (len(line_split) == 2) and (line_split[0] == load_key):
                    return line_split[1].lower().replace("\n", "")

    return None


def java_insecure_hash(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    graph = args.graph
    nodes = graph.nodes
    node = nodes[args.n_id]

    if "0" not in nodes:
        return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)

    if (node.get("variable_type") == "java.util.Properties") and (
        var_name := node.get("variable")
    ):
        args.triggers.add(f"loader_name__{var_name}")

    if (
        (val_id := node.get("value_id"))
        and (nodes[val_id].get("expression") == "getProperty")
        and (args_id := nodes[val_id].get("arguments_id"))
        and (load_key := get_first_string(graph, args_id))
        and (evaluate_properties(args, load_key[1:-1]) in INSECURE_HASHES)
    ):
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
