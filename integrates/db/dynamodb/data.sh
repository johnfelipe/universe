# shellcheck shell=bash

function get_git_email {
  HOME="${HOME_IMPURE}" git config user.email
}

function prepare_table_data {
  local email="${1}"
  local out="${2}"
  local table_name="${3}"
  local design_path="${4}"
  local i=0

  : \
    && jq -c \
      "{${table_name}: [.DataModel[].TableFacets[] | select(.FacetName) | {PutRequest: {Item: .TableData[]}}]}" \
      "${design_path}" > "${out}/database-design" \
    && items_len=$(jq ".${table_name} | length" "${out}/database-design") \
    && echo "${table_name} items qy: ${items_len}" \
    && while [ $((i * 25)) -lt "$items_len" ]; do
      local ilow=$((i * 25)) \
        && local ihigh=$(((i + 1) * 25)) \
        && jq -c "{${table_name}: .${table_name}[$ilow:$ihigh]}" \
          "${out}/database-design" | sed "s/__adminEmail__/${email}/g" \
          > "${out}/database-design-${table_name}${i}.json" \
        && i=$((i + 1))
    done
}

function main {
  local email="${GITLAB_USER_EMAIL:-$(get_git_email)}"
  local out="integrates/db/.data"

  : \
    && rm -rf "${out}" \
    && mkdir -p "${out}" \
    && echo '[INFO] Populating tables from database designs...' \
    && prepare_table_data "${email}" "${out}" "integrates_vms" "__argIntegratesVmsDbDesign__" \
    && prepare_table_data "${email}" "${out}" "fi_async_processing" "__argAsyncProcessingDbDesign__" \
    && echo "[INFO] Admin email for new DB: ${email}" \
    && echo "[INFO] Admin email for old DB: ${email}" \
    && for data in "__argDbData__/"*'.json'; do
      sed "s/__adminEmail__/${email}/g" "${data}" \
        > "${out}/$(basename "${data}")"
    done
}

main "${@}"
