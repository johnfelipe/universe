from .schema import (
    GROUP,
)
from datetime import (
    datetime,
)
from db_model.groups.types import (
    Group,
)
from decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_login,
)
from graphql.type.definition import (
    GraphQLResolveInfo,
)
from sessions import (
    domain as sessions_domain,
)


@GROUP.field("forcesExpDate")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
)
def resolve(
    parent: Group,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> str | None:
    if not isinstance(parent.agent_token, str):
        return None

    decoded_token = sessions_domain.decode_token(parent.agent_token)
    exp = decoded_token["exp"]
    exp_as_datetime = datetime.fromtimestamp(exp)
    return str(exp_as_datetime)
