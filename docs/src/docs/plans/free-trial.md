---
id: free-trial
title: Free trial
sidebar_label: Free trial
slug: /plans/free-trial
---

Self-enrollment is aimed at Fluid Attacks'
potential clients actively seeking
assistance to detect security
vulnerabilities in their systems.
We offer them a 21-day free trial
of our Continuous Hacking Machine
Plan (automated testing service),
in which they will have the
opportunity to explore our
[platform](/tech/platform/introduction).
Here is a short step-by-step guide
to enroll in this free trial.

First,
you need to go to Fluid Attacks'
[homepage](https://fluidattacks.com/).
There,
you will find the
Start free trial button.
Click on it to access the
free trial landing page.

![Fluid Attacks Homepage](https://res.cloudinary.com/fluid-attacks/image/upload/v1674728062/docs/web/enrollment/home_page.png)

You will be taken to the
official website of our
[platform](https://app.fluidattacks.com/).
There you need to log in with a Gmail,
Microsoft or Bitbucket business
email account and must not have
signed up for the free trial before.

![Log In](https://res.cloudinary.com/fluid-attacks/image/upload/v1686772308/docs/web/enrollment/platform.png)

Having logged in,
you will see a window where
you are asked to enter the
details about the repository
you want us to assess.

![Info Repositories](https://res.cloudinary.com/fluid-attacks/image/upload/v1674728624/docs/web/enrollment/set_up_repo.png)

> **Note:** If you add a repository by **Bitbucket**
> or clone via **HTTPS** with **GitHub,**
> you must replace the password with a personal token.
> To learn more,
> you can go to the following
> [link,](https://support.atlassian.com/bitbucket-cloud/docs/create-an-app-password/)
> which is the official Bitbucket documentation,
> and for GitHub documentation,
> click [here.](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token)

After providing the
requested information,
click on the
**Start your free trial** button.
From there,
it is a matter of waiting a
few hours while we clone your
repository and perform the
first security scan.

![Notification Free Trial](https://res.cloudinary.com/fluid-attacks/image/upload/v1654783880/docs/web/enrollment/enrollment_notif.png)

In the meantime,
you will receive an enrollment
notification via email,
where we welcome you to
the Machine Plan free trial.

![Enrollment Notification](https://res.cloudinary.com/fluid-attacks/image/upload/v1675096872/docs/web/enrollment/free_trial_start.png)

You can also access your
organization and group on
our platform and start exploring it.

![Access Platform](https://res.cloudinary.com/fluid-attacks/image/upload/v1672073424/docs/web/enrollment/group.png)

When your 21-day free trial
is about to run out,
you will receive messages to
decide if you want to extend
it for nine more days or
become a client of Fluid Attacks
to use Machine Plan without
limitations.
You can even talk to our staff
to know about and request the
comprehensive and more accurate
Squad Plan (automated and manual
testing service).

![End Free Trial](https://res.cloudinary.com/fluid-attacks/image/upload/v1675097510/docs/web/enrollment/free_trial_over.png)

If you found our services helpful
and want to continue with our
Machine or try our Squad plan,
you can click the talk to a
sales representative button.
